variable "name" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "location" {
  type = string
}

variable "replication_type" {
  type = string
}

variable "containers" {
  type = map(object({
    access_type = string
    ro          = list(string)
    rw          = list(string)
  }))
}

variable "cors_rule" {
  type = object({
    allowed_headers = list(string)
    allowed_methods = list(string)
    allowed_origins = list(string)
    exposed_headers = list(string)
    max_age_in_seconds = number
  })
  default = null
}